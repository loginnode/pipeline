var objectId=require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient;

var dbo;


MongoClient.connect("mongodb://localhost:27017/",{useNewUrlParser:true},function(err,db){
    if(err) throw err;
    dbo=db.db("mydb");
})
let register = (table,data) => {

    return new Promise((resolve, reject) =>{
  
       var col = dbo.collection(table);
  
       col.insertOne(data,function(err, result) {
  
            if(err)
            {
              reject(err);
            }
  
            resolve(result.insertedId);
  
        });
  
    });
  
  };
  let auth=(table,params)=>{
    return new Promise((resolve, reject) =>{
  
   
        var col = dbo.collection(table);
  
       col.find(params).toArray(function(err, result) {
            if(err)
            {
              reject(err);
            }
  
            resolve(result);
  
        });
     }); 
  }
  let updateData = (table,olddata,newdata) =>{

    return new Promise((resolve, reject) =>{
  
      var col = dbo.collection(table);
        
       col.updateOne(olddata,{$set:newdata},function(err, result) {
            if(err)
            {
              reject(err);
            }
  
            resolve(result);
  
        });
  
  
    });
  
  } 
  module.exports={
    register,
    auth,
    updateData
  }