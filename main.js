var express=require("express");
var app=express();
var bodyParser=require('body-parser');
const passport = require('passport');
var objectId=require('mongodb').ObjectID;
var MongoClient=require('mongodb').MongoClient;
var fs = require("fs");
var formidable = require('formidable');

const path = require('path');
var md5 = require('md5');
var LocalStrategy = require('passport-local').Strategy;
//var urlencodeedParser=bodyParser.urlencoded({extended:false});
app.set('view engine', 'ejs');

var promiseconnect=require('./promisedata');
	
var config = require('./config');
//app.use(express.static(path.join(__dirname, 'upload')));
app.use(express.static('public'));

var session = require('express-session');
var jwt = require('jsonwebtoken');

app.use(session({
    
    name: '_es_demo',
    secret: '1234',
    resave: false,

    saveUninitialized: false
}));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
    done(null, user);
 });
  
 passport.deserializeUser(function(user, done) {
    done(null, user);
 });

app.use(express.static('public'));
var loggedin=function(req,res,next){

    if(req.isAuthenticated()){
        next();
    }
    else{
        res.redirect('/log-in');
    }

}
app.get('/log-in',function(req,res){
    if(req.session.email){
        
        res.redirect('/home');
    }
    else{
        res.sendFile(__dirname+'/'+'login.html');
    }
    
   
})

app.get('/home',loggedin,function(req,res){
    
    if(req.session.passport.user.email){
        
        let email=req.session.passport.user.email;
        req.session.email=email;
        promiseconnect.auth('registration',{"email":email}).then((fromResolve)=>{
            
            if(req.session.passport.user.token){
                var token = req.session.passport.user.token;
                
                jwt.verify(token,config.secret,function(err, decoded){
                    if(err){
                        req.session.destroy();
                        res.redirect('/log-in');
                    }
                    else
                    {
                        if( req.session.message){
                            // fromResolve.push({"message":req.session.message});
             
                             res.render(__dirname+'/'+'home',{"data":fromResolve,"message":req.session.message,"type": req.session.type});
                         }
                         else{
                             //console.log(fromResolve); 
                             res.render(__dirname+'/'+'home',{"data":fromResolve,message:"Log in successfully",type:"success" });
                         }
                             
                    }

                });

            }
            
            
            
        }).catch((error)=>{
            res.redirect('/log-in');
        })
        
    }
    else{
        
        res.redirect('/log-in');
    }
})

app.get('/sign-up',function(req,res){
    
    res.sendFile(__dirname+'/'+'signup.html');
})
app.post('/register',function(req,res){
   
    var encryptedString =md5(req.body.password);
    var today = new Date();
    promiseconnect.auth('registration',{"email":req.body.email}).then((fromResolve)=>{
        if(fromResolve.length>0)
        {
            res.send('1000');
          
        }
        else{
            promiseconnect.register('registration',{name:req.body.name,phone:req.body.phone,email:req.body.email,password:encryptedString,address:req.body.address,date:today}).then((Resolve)=>{
                res.send('1001');
               
          
              }).catch((error)=>{
                
                res.send('1003');
       
              });
          

        }
        
        

    }).catch((error)=>{
        res.send('1004');
      
    });
   
    
})


// app.post('/login',function(req,res){
//     var encryptedString = md5(req.body.password);
    
//     req.session.email=req.body.email;
//     promiseconnect.auth('registration',{"email":req.body.email,"password":encryptedString}).then((fromResolve)=>{
       
//         if(fromResolve.length==1)
//         {
            
//             console.log(config.secret);
//           console.log("hii"+req.body.email);
//             var token = jwt.sign({email:req.body.email},config.secret , {
//                 expiresIn: 86400 // expires in 24 hours

//           });
//             console.log("hii");
//             console.log("token:="+token);
//             if(token){
//                 req.session.token=token;
//             }
//          res.send({auth: true,st:'1000'});
//         //res.send({ auth: true, token: token });
           
//         }
//         else{
//             res.send({auth: false,st:'1001'});
          
//         }
        
        

//     }).catch((error)=>{
      
//         res.send({auth: false,st:'1002'});
       
//     });
// })

app.post('/login', passport.authenticate('local',{
    failureRedirect:'/log-in',
    successRedirect:'/home'
}),
    function(req, res) 
    { 
        
      res.send('hi Parth'); 
   });
   passport.use(new LocalStrategy({usernameField:'email',passwordField:'password'},
 
    function(email,password, done) {
            var encryptedString = md5(password);
    
   
    promiseconnect.auth('registration',{"email":email,"password":encryptedString}).then((fromResolve)=>{
        if(fromResolve.length==1)
                {
                    
                    
                    var token = jwt.sign({email:email},config.secret , {
                        expiresIn: 86400 // expires in 24 hours
        
                  });
                   
                   
                done(null,{
                    email:email,
                    token:token
                })
                
                   
                }
                else{
                    done(null,false);
                  
                }
                
                
        
            }).catch((error)=>{
              
                done(null,false);
               
            }); 
        
      
        

      
       }
   ));
       
  // app.get('/example',loggedin,function(req,res){
//     res.send(req.session.passport.user.email);
//    //res.json(req.session);
// });

app.post('/update',function(req,res){
    var form = new formidable.IncomingForm();
            form.parse(req, function (err, fields, files) {
                if(files.profile.name){
                    var oldpath = files.profile.path;
                var newpath = __dirname+'/upload/'+files.profile.name;
                fs.rename(oldpath, newpath, function (err) {
                    if (err) 
                    {
                   
                      req.session.type="error";
                      req.session.message="Your profile is not update";
                      res.redirect('/home');
                    }
                    else{
                        promiseconnect.updateData('registration',{"email":fields.email}, {"name":fields.name,"address":fields.address,"profile":files.profile.name,"phone":fields.phone,"country":fields.country,"state":fields.state,"city":fields.city}).then((fromResolve)=>{
                            req.session.type="success";
                            req.session.message="Your profile is update";                            
                            res.redirect('/home');
                    
                        }).catch((error)=>{
                           
                            req.session.message="error";
                            req.session.message="Your profile is not update";
                            res.redirect('/home');
                        });

                        
                    }
                    
                });

                }
                else{
                    promiseconnect.updateData('registration',{"email":fields.email}, {"name":fields.name,"address":fields.address,"phone":fields.phone,"country":fields.country,"state":fields.state,"city":fields.city}).then((fromResolve)=>{
                        req.session.type="success";
                        req.session.message="Your profile is update"; 
                        res.redirect('/home');
                
                    }).catch((error)=>{
                       
                        req.session.message="error";
                        req.session.message="Your profile is not update";
                        res.redirect('/home');
                    });

                }
                
            }); 
  
})




app.listen(8080,function(req,res){
    console.log("Project run in port 8080");
})
