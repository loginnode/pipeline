function chkemail(){
var email=$("#email").val();
var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

if(!re.test(email)){
  $('#erroremail').html('Enter your proper email');
  return false;
}
else{
  $('#erroremail').html('');
}
}
function chknumber()
{
var phone=$('#phone').val();
var re=/^([0-9]{10})*$/;
if(!re.test(phone)){
  $('#errorphone').html('Enter your proper phone no.');
  return false;
}
else{
  $('#errorphone').html('');
}
}
function chkname()
{
var name=$('#name').val();
var re=/^([a-zA-Z ])*$/;
if(!re.test(name)){
  $('#errorname').html('Enter your proper name');
  return false;
}
else{
  $('#errorname').html('');
}
}
function passwordchk()
{

  var name=$('#name').val();
  var phone=$('#phone').val();
  var address=$('#address').val();
  var email=$('#email').val();
 
 
  if(name=="" ||phone=="" ||address==""||email=="")
  {
    if(name=="")
    {
      $('#errorname').html('Enter your name');
     
    }
    else{
      $('#errorname').html('');

    }
    if(phone=="")
    {
      $('#errorphone').html('Enter your phone no.');
      
    }
    else{
      $('#errorphone').html('');

    }
    if(address=="")
    {
      $('#erroraddress').html('Enter your address');
      
    }
    else{
      $('#erroraddress').html('');

    }
    if(email=="")
    {
      $('#erroremail').html('Enter your email');
      
    }
    else{
      $('#erroremail').html('');

    }
   
   
    return false;
  }
  else if(name!="" && phone!="" && address!="" && email!="" )
   {
   
      $('#errorname').html('');
      $('#errorphone').html('');
      $('#erroremail').html('');
      $('#erroraddress').html('');
      
    return true;
    

    
  }
  
  
  
}

